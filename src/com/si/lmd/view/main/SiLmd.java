/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.si.lmd.view.main;

import com.si.lmd.ctrl.connection.ConnectionDB;
import com.si.lmd.view.master.ViewPegawai;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import javax.swing.JDesktopPane;
import javax.swing.JFrame;
import static javax.swing.JFrame.EXIT_ON_CLOSE;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

/**
 *
 * @author TRI
 */
public final class SiLmd extends JFrame implements ActionListener{
    
    public Connection conn = null;
    
    public JDesktopPane desktop = new JDesktopPane();
    
    public JMenuBar menuBar = new JMenuBar();
    
    public JMenu menuFile = new JMenu("File");
    public JMenuItem menuItemExit = new JMenuItem("Exit");
        
    public JMenu menuMaster = new JMenu("Master");
    public JMenuItem menuItemPegawai = new JMenuItem("Pegawai");
    public JMenuItem menuItemPekerjaan = new JMenuItem("Pekerjaan");
    public JMenuItem menuItemRincian = new JMenuItem("Rincian");
    public JMenuItem menuItemGambar = new JMenuItem("Gambar");   
     
    
    public JMenu menuLaporan = new JMenu("Laporan");
    public JMenuItem menuLaporanPerBulan = new JMenuItem("Laporan Per Bulan");

    
    public SiLmd(){                                      
        this.component();
        this.connection();   
    }
    
    public void component(){
        
        this.setJMenuBar(menuBar);
        
        menuBar.add(menuFile);
        menuFile.add(menuItemExit);
        menuItemExit.addActionListener(this);
                
        menuBar.add(menuMaster);
        menuMaster.add(menuItemPegawai);
        menuMaster.add(menuItemPekerjaan);
        menuMaster.add(menuItemRincian);
        menuMaster.add(menuItemGambar);        
        menuItemPegawai.addActionListener(this);
        menuItemPekerjaan.addActionListener(this);
        menuItemRincian.addActionListener(this);
        menuItemGambar.addActionListener(this);                             
        
        menuBar.add(menuLaporan);
        menuLaporan.add(menuLaporanPerBulan);
        menuLaporanPerBulan.addActionListener(this);  
        
        this.setContentPane(desktop);
        
    }
    
    
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        SiLmd silmd = new SiLmd();  
        silmd.setTitle("Sistem Informasi Listrik Masuk Desa");   
        silmd.setExtendedState(silmd.getExtendedState() | SiLmd.MAXIMIZED_BOTH);
        silmd.setDefaultCloseOperation(EXIT_ON_CLOSE);
        silmd.setVisible(true);         
    }
    
    
    public void connection(){
        ConnectionDB connDB = new ConnectionDB();
        conn = connDB.ConnectionDB();
        if(conn!=null){
            System.out.println("Connection Succes");
        }else{
            System.out.println("Connection Failed");
        }        
    } 

    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource()==menuItemExit){
            System.exit(0);            
        }else if(e.getSource()==menuItemPegawai){
            System.out.println("menuItemPegawai");
            ViewPegawai vPegawai = new ViewPegawai(conn);
            desktop.add(vPegawai);                    
        }else if(e.getSource()==menuItemPekerjaan){
            System.out.println("menuItemPekerjaan");
        }else if(e.getSource()==menuItemRincian){
            System.out.println("menuItemRincian");
        }else if(e.getSource()==menuItemGambar){
            System.out.println("menuItemGambar");
        }
    }    

    public Connection getConn() {
        return conn;
    }

    public void setConn(Connection conn) {
        this.conn = conn;
    }

}
