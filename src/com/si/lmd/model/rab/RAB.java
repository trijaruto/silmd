/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.si.lmd.model.rab;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author TRI
 */
public class RAB implements Serializable{
    
    private long autoindex;
    private String nomor;
    private String year;
    private String paket;
    private String lokasi;
    private int desa;
    private int kecamatan;
    private String sumberdana;
    private String penawar;
    private String doklang;
    private String cabang;
    private int halaman;
    private String perhitungan;
    private int ppk;
    private String tempat;
    private Date tanggal;
    
    public RAB(){
        
    }

    public long getAutoindex() {
        return autoindex;
    }

    public void setAutoindex(long autoindex) {
        this.autoindex = autoindex;
    }

    public String getNomor() {
        return nomor;
    }

    public void setNomor(String nomor) {
        this.nomor = nomor;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getPaket() {
        return paket;
    }

    public void setPaket(String paket) {
        this.paket = paket;
    }

    public String getLokasi() {
        return lokasi;
    }

    public void setLokasi(String lokasi) {
        this.lokasi = lokasi;
    }

    public int getDesa() {
        return desa;
    }

    public void setDesa(int desa) {
        this.desa = desa;
    }

    public int getKecamatan() {
        return kecamatan;
    }

    public void setKecamatan(int kecamatan) {
        this.kecamatan = kecamatan;
    }

    public String getSumberdana() {
        return sumberdana;
    }

    public void setSumberdana(String sumberdana) {
        this.sumberdana = sumberdana;
    }

    public String getPenawar() {
        return penawar;
    }

    public void setPenawar(String penawar) {
        this.penawar = penawar;
    }

    public String getDoklang() {
        return doklang;
    }

    public void setDoklang(String doklang) {
        this.doklang = doklang;
    }

    public String getCabang() {
        return cabang;
    }

    public void setCabang(String cabang) {
        this.cabang = cabang;
    }

    public int getHalaman() {
        return halaman;
    }

    public void setHalaman(int halaman) {
        this.halaman = halaman;
    }

    public String getPerhitungan() {
        return perhitungan;
    }

    public void setPerhitungan(String perhitungan) {
        this.perhitungan = perhitungan;
    }

    public int getPpk() {
        return ppk;
    }

    public void setPpk(int ppk) {
        this.ppk = ppk;
    }

    public String getTempat() {
        return tempat;
    }

    public void setTempat(String tempat) {
        this.tempat = tempat;
    }

    public Date getTanggal() {
        return tanggal;
    }

    public void setTanggal(Date tanggal) {
        this.tanggal = tanggal;
    }
    
    
}
