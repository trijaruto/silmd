/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.si.lmd.model.rab;

import java.io.Serializable;

/**
 *
 * @author TRI
 */
public class RABRincian implements Serializable{
    
    private long autoindex;
    private long RABPekerjaan;
    private long indexRincian;
    private long indexGambar;
    private double volume;
    private String satuan;
    private double harga;
    
    public RABRincian(){
        
    }

    public long getAutoindex() {
        return autoindex;
    }

    public void setAutoindex(long autoindex) {
        this.autoindex = autoindex;
    }

    public long getRABPekerjaan() {
        return RABPekerjaan;
    }

    public void setRABPekerjaan(long RABPekerjaan) {
        this.RABPekerjaan = RABPekerjaan;
    }

    public long getIndexRincian() {
        return indexRincian;
    }

    public void setIndexRincian(long indexRincian) {
        this.indexRincian = indexRincian;
    }

    public long getIndexGambar() {
        return indexGambar;
    }

    public void setIndexGambar(long indexGambar) {
        this.indexGambar = indexGambar;
    }

    public double getVolume() {
        return volume;
    }

    public void setVolume(double volume) {
        this.volume = volume;
    }

    public String getSatuan() {
        return satuan;
    }

    public void setSatuan(String satuan) {
        this.satuan = satuan;
    }

    public double getHarga() {
        return harga;
    }

    public void setHarga(double harga) {
        this.harga = harga;
    }
    
    
    
    
}
