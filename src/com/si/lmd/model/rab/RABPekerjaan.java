/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.si.lmd.model.rab;

import java.io.Serializable;

/**
 *
 * @author TRI
 */
public class RABPekerjaan implements Serializable{
    
    private long autoindex;
    private long indexRAB;    
    private int indexPekerjaan;
    private double total;   
    
    public RABPekerjaan(){
        
    }

    public long getAutoindex() {
        return autoindex;
    }

    public void setAutoindex(long autoindex) {
        this.autoindex = autoindex;
    }

    public long getIndexRAB() {
        return indexRAB;
    }

    public void setIndexRAB(long indexRAB) {
        this.indexRAB = indexRAB;
    }

    public int getIndexPekerjaan() {
        return indexPekerjaan;
    }

    public void setIndexPekerjaan(int indexPekerjaan) {
        this.indexPekerjaan = indexPekerjaan;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }
    
    
    
}
