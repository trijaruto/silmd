/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.si.lmd.model.master;

import java.io.Serializable;

/**
 *
 * @author TRI
 */
public class Pekerjaan implements Serializable{
    
    private long autoindex;
    private String code;
    private String nama;
    
    public Pekerjaan(){
        
    }

    public long getAutoindex() {
        return autoindex;
    }

    public void setAutoindex(long autoindex) {
        this.autoindex = autoindex;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }
    
    
}
