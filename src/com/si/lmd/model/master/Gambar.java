/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.si.lmd.model.master;

import java.io.Serializable;

/**
 *
 * @author TRI
 */
public class Gambar implements Serializable{
    
    private long autoindex;
    private String code;
    private String name;
    private Byte imgPicture;
    
    public Gambar(){
        
    }

    public long getAutoindex() {
        return autoindex;
    }

    public void setAutoindex(long autoindex) {
        this.autoindex = autoindex;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Byte getImgPicture() {
        return imgPicture;
    }

    public void setImgPicture(Byte imgPicture) {
        this.imgPicture = imgPicture;
    }
    
    
    
}
