/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.si.lmd.ctrl.connection;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author TRI
 */
public class ConnectionDB implements Serializable{
    
    public Connection conn = null;
    
    public ConnectionDB(){
        
    }
    
    public Connection ConnectionDB(){
        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            System.out.println("Where is your PostgreSQL JDBC Driver? "
                                + "Include in your library path!");
            e.printStackTrace();         
        }
        
        try {
            conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/silmd", "postgres", "postgres");
        } catch (SQLException e) {
            System.out.println("Connection Failed! Check output console");
            e.printStackTrace();          
        }
        
        return conn;
    }
    
    
}
