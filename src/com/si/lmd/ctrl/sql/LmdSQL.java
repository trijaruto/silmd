/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.si.lmd.ctrl.sql;

import com.si.lmd.model.master.Pegawai;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.DefaultListModel;

/**
 *
 * @author TRI
 */
public class LmdSQL implements ILmd{
    
    private PreparedStatement pstm = null;
    private Statement stm = null;
    private ResultSet rs = null;
    private long autoindex = 0;
            
    public LmdSQL(){
        
    }
    
    public long getMaxIndex(String tablename, Connection conn) throws SQLException {        
        try {
            stm = conn.createStatement();
            rs = stm.executeQuery("SELECT MAX(autoindex) as maxindex FROM " + tablename);
            rs.next();
            return rs.getLong("maxindex");
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    @Override
    public long onSaveMasterPegawai(Pegawai pegawai, Connection conn) throws SQLException {                   
        try {
            pstm = conn.prepareStatement("INSERT INTO pegawai( " +
                        " nip, nama, jabatan, eselon, golongan) " +
                        " VALUES (?, ?, ?, ?, ?);");
            
            int col = 1;            
            pstm.setString(col++, pegawai.getNip());
            pstm.setString(col++, pegawai.getName());
            pstm.setString(col++, pegawai.getJabatan());
            pstm.setString(col++, pegawai.getEselon());
            pstm.setString(col++, pegawai.getGolongan());

            pstm.execute();
            
            autoindex = getMaxIndex("pegawai", conn);
                        
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if (pstm != null) {
                pstm.close();
            }           
        }
        
        return autoindex;
    }
    
    @Override
    public void onUpdateMasterPegawai(Pegawai pegawai, Connection conn) throws Exception {
        try {
            pstm = conn.prepareStatement("UPDATE pegawai "
                    + "SET nip=?, nama=?, jabatan=?, eselon=?, golongan=?"
                    + "WHERE autoindex = "+pegawai.getAutoindex());
            
            int col = 1;            
            pstm.setString(col++, pegawai.getNip());
            pstm.setString(col++, pegawai.getName());
            pstm.setString(col++, pegawai.getJabatan());
            pstm.setString(col++, pegawai.getEselon());
            pstm.setString(col++, pegawai.getGolongan());

            pstm.executeUpdate();            
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if (pstm != null) {
                pstm.close();
            }           
        }       
    }

    @Override
    public void onDeleteMasterPegawaiByAutoindex(long autoindex, Connection conn) throws Exception {
        try {
            System.out.println("autoindex "+autoindex);
            stm = conn.createStatement();
            stm.executeUpdate("DELETE FROM pegawai "
                    + "WHERE "
                    + "autoindex = "+autoindex);

        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if (stm != null) {
                stm.close();
            }
        }
    }
    
    
    @Override
    public DefaultListModel onLoadPegawai(Connection conn) throws SQLException {        
        DefaultListModel obj = new DefaultListModel();
        try {
            stm = conn.createStatement();
            
            String sql = "SELECT * FROM pegawai;";
            rs = stm.executeQuery(sql);

            while (rs.next()) {               
                Pegawai pegawai = new Pegawai();
                pegawai.setAutoindex(rs.getLong("autoindex"));
                pegawai.setNip(rs.getString("nip"));
                pegawai.setName(rs.getString("nama"));
                pegawai.setJabatan(rs.getString("jabatan"));
                pegawai.setGolongan(rs.getString("eselon"));
                pegawai.setEselon(rs.getString("golongan"));                
                obj.addElement(pegawai);
            }

            return obj;
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            
            if (stm != null) {
                stm.close();
            }
        }        
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    

    public PreparedStatement getPstm() {
        return pstm;
    }

    public void setPstm(PreparedStatement pstm) {
        this.pstm = pstm;
    }

    public Statement getStm() {
        return stm;
    }

    public void setStm(Statement stm) {
        this.stm = stm;
    }

    public ResultSet getRs() {
        return rs;
    }

    public void setRs(ResultSet rs) {
        this.rs = rs;
    }

    public long getAutoindex() {
        return autoindex;
    }

    public void setAutoindex(long autoindex) {
        this.autoindex = autoindex;
    }

    

    
    
    
    
    
    
}
