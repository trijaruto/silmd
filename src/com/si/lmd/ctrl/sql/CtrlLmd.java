/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.si.lmd.ctrl.sql;

import com.si.lmd.model.master.Pegawai;
import java.sql.Connection;
import java.sql.SQLException;
import javax.swing.DefaultListModel;

/**
 *
 * @author TRI
 */
public class CtrlLmd implements ILmd{


    @Override
    public long onSaveMasterPegawai(Pegawai pegawai, Connection conn) throws Exception {
        try {
            int currentLevel = conn.getTransactionIsolation();
            conn.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
            conn.setAutoCommit(false);
            
            LmdSQL sql = new LmdSQL();
            
            conn.commit();
            conn.setAutoCommit(true);
            conn.setTransactionIsolation(currentLevel);
            
            return sql.onSaveMasterPegawai(pegawai, conn);
        } catch (SQLException ex) {           
            throw new Exception("CtrlLmd : Gagal onSaveMasterPegawai\n" + ex.toString());           
        }
    }

    @Override
    public void onUpdateMasterPegawai(Pegawai pegawai, Connection conn) throws Exception {
        try {
            int currentLevel = conn.getTransactionIsolation();
            conn.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
            conn.setAutoCommit(false);
            
            LmdSQL sql = new LmdSQL();
            sql.onUpdateMasterPegawai(pegawai, conn);
            
            conn.commit();
            conn.setAutoCommit(true);
            conn.setTransactionIsolation(currentLevel);          
        } catch (SQLException ex) {           
            throw new Exception("CtrlLmd : Gagal onUpdateMasterPegawai\n" + ex.toString());           
        }
    }

    @Override
    public void onDeleteMasterPegawaiByAutoindex(long autoindex, Connection conn) throws Exception {
        try {           
            LmdSQL sql = new LmdSQL();            
            sql.onDeleteMasterPegawaiByAutoindex(autoindex, conn);
        } catch (SQLException ex) {           
            throw new Exception("CtrlLmd : Gagal onDeleteMasterPegawaiByAutoindex\n" + ex.toString());           
        }
    }

    @Override
    public DefaultListModel onLoadPegawai(Connection conn) throws Exception {
        try {           
            LmdSQL sql = new LmdSQL();            
            return sql.onLoadPegawai(conn);
        } catch (SQLException ex) {           
            throw new Exception("CtrlLmd : Gagal onSaveMasterPegawai\n" + ex.toString());           
        }
    }
    
    
    
}
