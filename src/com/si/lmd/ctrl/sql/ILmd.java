/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.si.lmd.ctrl.sql;

import com.si.lmd.model.master.Pegawai;
import java.sql.Connection;
import javax.swing.DefaultListModel;

/**
 *
 * @author TRI
 */
public interface ILmd {
    
    /*MASTER*/
    
    //PEGAWAI
    public long onSaveMasterPegawai(Pegawai pegawai, Connection conn) throws Exception;   
    public void onUpdateMasterPegawai(Pegawai pegawai, Connection conn) throws Exception;
    public void onDeleteMasterPegawaiByAutoindex(long autoindex, Connection conn) throws Exception;            
    public DefaultListModel onLoadPegawai(Connection conn) throws Exception;   
    
}
